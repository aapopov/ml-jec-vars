#include <Analysis/MLJECVars/interface/JetFilter.h>

#include <cmath>
#include <cstdlib>
#include <vector>

#include <gsl/gsl_interp2d.h>
#include <TFile.h>
#include <TH2.h>


/**
 * \brief Implements bicubic interpolation of the logarithm of the jet selection
 * efficiency
 */
class Interpolator {
 public:
  Interpolator(TH2 const *filter_hist);
  ~Interpolator();
  double Interpolate(double pt, double eta) const;

 private:
  std::vector<double> knots_x_, knots_y_;
  std::vector<double> ref_values_;
  gsl_interp_accel *x_acc_, *y_acc_;
  gsl_interp2d *spline_;
};


Interpolator::Interpolator(TH2 const *filter_hist)
    : x_acc_{nullptr}, y_acc_{nullptr},
      spline_{nullptr} {
  TAxis const *axis = filter_hist->GetXaxis();
  int const nx = axis->GetNbins();
  knots_x_.reserve(nx);
  for (int bin = 1; bin <= nx; ++bin)
    knots_x_.emplace_back(axis->GetBinCenter(bin));

  axis = filter_hist->GetYaxis();
  int const ny = axis->GetNbins();
  knots_y_.reserve(ny);
  for (int bin = 1; bin <= ny; ++bin)
    knots_y_.emplace_back(axis->GetBinCenter(bin));

  ref_values_.resize(nx * ny);
  for (int i = 0; i < nx; ++i)
    for (int j = 0; j < ny; ++j)
      ref_values_[i + j * nx] = filter_hist->GetBinContent(i + 1, j + 1);

  x_acc_ = gsl_interp_accel_alloc();
  y_acc_ = gsl_interp_accel_alloc();
  spline_ = gsl_interp2d_alloc(gsl_interp2d_bicubic, nx, ny);
  gsl_interp2d_init(
      spline_, knots_x_.data(), knots_y_.data(),
      ref_values_.data(), nx, ny);
}


Interpolator::~Interpolator() {
  gsl_interp2d_free(spline_);
  gsl_interp_accel_free(x_acc_);
  gsl_interp_accel_free(y_acc_);
}


double Interpolator::Interpolate(double p, double eta) const {
  double x = std::asinh(p / 100.);
  if (x < knots_x_.front())
    x = knots_x_.front();
  else if (x > knots_x_.back())
    x = knots_x_.back();

  double y = std::abs(eta);
  if (y < knots_y_.front())  // The centre of the first bin has |eta| > 0
    y = knots_y_.front();
  else if (y > knots_y_.back())
    y = knots_y_.back();

  return gsl_interp2d_eval(
      spline_, knots_x_.data(), knots_y_.data(), ref_values_.data(),
      x, y, x_acc_, y_acc_);
}


JetFilter::JetFilter(std::string const &path) {
  TFile hist_file{path.c_str()};
  interp_b_.reset(new Interpolator(dynamic_cast<TH2 *>(hist_file.Get("b"))));
  interp_c_.reset(new Interpolator(dynamic_cast<TH2 *>(hist_file.Get("c"))));
  interp_uds_.reset(new Interpolator(dynamic_cast<TH2 *>(hist_file.Get("uds"))));
  interp_g_.reset(new Interpolator(dynamic_cast<TH2 *>(hist_file.Get("g"))));
  hist_file.Close();
}


JetFilter::~JetFilter() {}


double JetFilter::GetEfficiency(reco::GenJet const &jet, int hadron_flavor,
                                int parton_flavor) const {
  int const comb_flavor =
      (hadron_flavor != 0) ? hadron_flavor : std::abs(parton_flavor);
  Interpolator *interp;
  if (comb_flavor == 5)
    interp = interp_b_.get();
  else if (comb_flavor == 4)
    interp = interp_c_.get();
  else if (comb_flavor == 21 or comb_flavor == 0)
    interp = interp_g_.get();
  else
    interp = interp_uds_.get();

  double const eff = std::exp(interp->Interpolate(jet.p(), jet.eta()));
  return std::min(eff, 1.);
}

