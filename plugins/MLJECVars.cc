#include "MLJECVars.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <stdexcept>

#include <DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h>
#include <DataFormats/GeometryVector/interface/GlobalVector.h>
#include <DataFormats/Math/interface/deltaPhi.h>
#include <DataFormats/Math/interface/deltaR.h>
#include <DataFormats/PatCandidates/interface/PackedCandidate.h>
#include <FWCore/Framework/interface/MakerMacros.h>
#include <RecoVertex/VertexPrimitives/interface/ConvertToFromReco.h>
#include <RecoVertex/VertexPrimitives/interface/VertexState.h>
#include <RecoVertex/VertexTools/interface/VertexDistance3D.h>


constexpr decltype(MLJECVars::energy_rings_dr_) MLJECVars::energy_rings_dr_;


MLJECVars::MLJECVars(edm::ParameterSet const &cfg)
    : MLJECBase(cfg) {}


void MLJECVars::fillDescriptions(edm::ConfigurationDescriptions &descriptions) {
  auto const desc = CreateDescription();
  descriptions.add("mlJECVars", desc);
}


void MLJECVars::AddBranches(TTree *tree) {
  tree->Branch("mult_mu", &mult_mu_);
  tree->Branch("mult_el", &mult_el_);
  tree->Branch("mult_ph", &mult_ph_);
  tree->Branch("mult_ch_had", &mult_ch_had_);
  tree->Branch("mult_ne_had", &mult_ne_had_);
  tree->Branch("mult_hf_em", &mult_hf_em_);
  tree->Branch("mult_hf_had", &mult_hf_had_);

  tree->Branch("energy_frac_mu", &energy_frac_mu_);
  tree->Branch("energy_frac_el", &energy_frac_el_);
  tree->Branch("energy_frac_ph", &energy_frac_ph_);
  tree->Branch("energy_frac_ch_had", &energy_frac_ch_had_);
  tree->Branch("energy_frac_ne_had", &energy_frac_ne_had_);
  tree->Branch("energy_frac_hf_em", &energy_frac_hf_em_);
  tree->Branch("energy_frac_hf_had", &energy_frac_hf_had_);

  tree->Branch("pt_d", &pt_d_);
  tree->Branch("mean_dr2", &mean_dr2_);
  tree->Branch("pt_frac_lead", &pt_frac_lead_);
  tree->Branch("pt_frac_lead_ch", &pt_frac_lead_ch_);
  tree->Branch("etaphi_axis_major", &etaphi_axis_major_);
  tree->Branch("etaphi_axis_minor", &etaphi_axis_minor_);
  tree->Branch("pileup_beta", &pileup_beta_);
  tree->Branch("pull_magnitude", &pull_magnitude_);

  for (int ring = 0; ring < int(energy_rings_all_.size()); ++ring) {
    std::string const prefix = "energy_frac_ring" + std::to_string(ring + 1);
    tree->Branch(prefix.c_str(), &energy_rings_all_[ring]);
    tree->Branch((prefix + "_mu").c_str(), &energy_rings_mu_[ring]);
    tree->Branch((prefix + "_el").c_str(), &energy_rings_el_[ring]);
    tree->Branch((prefix + "_ph").c_str(), &energy_rings_ph_[ring]);
    tree->Branch((prefix + "_ch_had").c_str(), &energy_rings_ch_had_[ring]);
    tree->Branch((prefix + "_ne_had").c_str(), &energy_rings_ne_had_[ring]);
    tree->Branch((prefix + "_hf_em").c_str(), &energy_rings_hf_em_[ring]);
    tree->Branch((prefix + "_hf_had").c_str(), &energy_rings_hf_had_[ring]);
  }

  tree->Branch("sv_mass", &sv_mass_);
  tree->Branch("sv_pt_frac", &sv_pt_frac_);
  tree->Branch("sv_distance", &sv_distance_);
  tree->Branch("sv_significance", &sv_significance_);
  tree->Branch("sv_num_tracks", &sv_num_tracks_);
}


std::tuple<double, double> MLJECVars::EtaPhiAxes(pat::Jet const &jet) {
  // Components of (Et-weighted) covariance matrix in (eta, phi) plane
  double const etaeta = jet.etaetaMoment();
  double const phiphi = jet.phiphiMoment();
  double const etaphi = jet.etaphiMoment();

  // Compute eigenvalues analytically
  double const d = std::sqrt(
      std::pow(etaeta - phiphi, 2) + 4 * etaphi * etaphi);
  double const lambda1 = std::max((etaeta + phiphi + d) / 2, 0.);
  double const lambda2 = std::max((etaeta + phiphi - d) / 2, 0.);

  return {std::sqrt(lambda1), std::sqrt(lambda2)};
}


void MLJECVars::FillEnergyRings(pat::Jet const &jet) {
  for (auto &r : {
      &energy_rings_mu_, &energy_rings_el_, &energy_rings_ph_,
      &energy_rings_ch_had_, &energy_rings_ne_had_,
      &energy_rings_hf_em_, &energy_rings_hf_had_, &energy_rings_all_})
    r->fill(0.);
  double jet_energy = 0.;

  for (int i = 0; i < int(jet.numberOfDaughters()); ++i) {
    reco::Candidate const *p = jet.daughter(i);
    double const energy = p->energy();
    jet_energy += energy;

    double const dr = reco::deltaR(jet, *p);
    int const ring = std::upper_bound(
        energy_rings_dr_.begin(), energy_rings_dr_.end(), dr)
      - energy_rings_dr_.begin();

    int const abs_pdg_id = std::abs(p->pdgId());
    energy_rings_all_[ring] += energy;
    switch (abs_pdg_id) {
      case 13:
        energy_rings_mu_[ring] += energy;
        break;
      case 11:
        energy_rings_el_[ring] += energy;
        break;
      case 22:
        energy_rings_ph_[ring] += energy;
        break;
      case 211:
        energy_rings_ch_had_[ring] += energy;
        break;
      case 130:
        energy_rings_ne_had_[ring] += energy;
        break;
      case 1:
        energy_rings_hf_had_[ring] += energy;
        break;
      case 2:
        energy_rings_hf_em_[ring] += energy;
        break;
      default:
        throw std::runtime_error("Unknown PDG ID for jet constituent.");
    }
  }

  for (auto &r : {
      &energy_rings_mu_, &energy_rings_el_, &energy_rings_ph_,
      &energy_rings_ch_had_, &energy_rings_ne_had_,
      &energy_rings_hf_em_, &energy_rings_hf_had_, &energy_rings_all_})
    for (auto &v : *r)
      v /= jet_energy;
}


void MLJECVars::FillJet(
    pat::Jet const &jet, reco::Vertex const *pv,
    edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices) {
  mult_mu_ = jet.muonMultiplicity();
  mult_el_ = jet.electronMultiplicity();
  mult_ph_ = jet.photonMultiplicity();
  mult_ch_had_ = jet.chargedHadronMultiplicity();
  mult_ne_had_ = jet.neutralHadronMultiplicity();
  mult_hf_em_ = jet.HFEMMultiplicity();
  mult_hf_had_ = jet.HFHadronMultiplicity();

  energy_frac_mu_ = jet.muonEnergyFraction();
  energy_frac_el_ = jet.electronEnergyFraction();
  energy_frac_ph_ = jet.photonEnergyFraction();
  energy_frac_ch_had_ = jet.chargedHadronEnergyFraction();
  energy_frac_ne_had_ = jet.neutralHadronEnergyFraction();
  energy_frac_hf_em_ = jet.HFEMEnergyFraction();
  energy_frac_hf_had_ = jet.HFHadronEnergyFraction();

  int const num_daughters = jet.numberOfDaughters();
  assert(num_daughters > 0);
  double sum_pt2 = 0., sum_dr2_pt2 = 0.;
  double max_pt_daughter = 0., max_pt_ch_daughter = 0.;
  for (int i = 0; i < num_daughters; ++i) {
    auto const daughter = jet.daughter(i);
    double const pt2 = std::pow(daughter->pt(), 2);
    sum_pt2 += pt2;
    sum_dr2_pt2 += reco::deltaR2(jet, *daughter) * pt2;
    if (daughter->pt() > max_pt_daughter)
      max_pt_daughter = daughter->pt();
    if (IsCharged(*daughter) and daughter->pt() > max_pt_ch_daughter)
      max_pt_ch_daughter = daughter->pt();
  }

  // Variants of variables from JME-18-001
  double const raw_pt = jet.correctedP4("Uncorrected").pt();

  pt_d_ = std::sqrt(sum_pt2) / raw_pt;
  mean_dr2_ = sum_dr2_pt2 / sum_pt2;
  pt_frac_lead_ = max_pt_daughter / raw_pt;
  pt_frac_lead_ch_ = max_pt_ch_daughter / raw_pt;
  std::tie(etaphi_axis_major_, etaphi_axis_minor_) = EtaPhiAxes(jet);
  pileup_beta_ = PileupBeta(jet);
  pull_magnitude_ = PullMagnitude(jet);

  FillEnergyRings(jet);
  SaveSecondaryVertex(jet, pv, secondary_vertices);
}


double MLJECVars::PileupBeta(pat::Jet const &jet) {
  double sum_pt = 0., sum_pt_vertex = 0.;
  for (int i = 0; i < int(jet.numberOfDaughters()); ++i) {
    auto cand = dynamic_cast<pat::PackedCandidate const *>(jet.daughter(i));
    sum_pt += cand->pt();
    if (not IsCharged(*cand))
      continue;

    // If the candidate is associated with other primary vertex than the leading
    // one, skip it
    if (cand->vertexRef().key() != 0)
      continue;

    // Check the quality of association. Similarly to what is done [1] for
    // JME-18-001, use an association based on dz, but the exact requirement is
    // not exactly the same.
    // [1] https://github.com/cms-sw/cmssw/blob/9834f5dc9ff342ddef08b73d6c294cad36575772/RecoJets/JetProducers/src/PileupJetIdAlgo.cc#L509-L510
    auto const min_quality =
        pat::PackedCandidate::PVAssociationQuality::CompatibilityDz;
    if (cand->pvAssociationQuality() < min_quality)
      continue;
    sum_pt_vertex += cand->pt();
  }
  return sum_pt_vertex / sum_pt;
}


double MLJECVars::PullMagnitude(pat::Jet const &jet) {
  double sum_eta = 0., sum_phi = 0.;
  for (int i = 0; i < int(jet.numberOfDaughters()); ++i) {
    auto const daughter = jet.daughter(i);
    double const deta = daughter->eta() - jet.eta();
    double const dphi = reco::deltaPhi(daughter->phi(), jet.phi());
    double const r = std::hypot(deta, dphi);
    sum_eta += r * deta * daughter->pt();
    sum_phi += r * dphi * daughter->pt();
  }
  return std::hypot(sum_eta, sum_phi) / jet.pt();
}


void MLJECVars::SaveSecondaryVertex(
    pat::Jet const &jet, reco::Vertex const *pv,
    edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices) {
  sv_mass_ = sv_pt_frac_ = sv_distance_ = sv_significance_ = 0.;
  sv_num_tracks_ = 0;
  if (not pv)
    return;

  // Code below is adapted from [1] as recommended in [2]
  // [1] https://github.com/cms-nanoAOD/cmssw/blob/419d33be023f9f2a4c56ef4b851552d2d228600a/PhysicsTools/NanoAOD/plugins/BJetEnergyRegressionVarProducer.cc#L242-L257
  // [2] https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2017?rev=23#Secondary_Vertices_and_V0s
  GlobalVector const jet_direction(jet.px(), jet.py(), jet.pz());
  VertexDistance3D distance_calc;
  double max_significance = -std::numeric_limits<double>::infinity();

  for (auto const &sv : secondary_vertices) {
    GlobalVector const flight_direction(
        sv.vertex().x() - pv->x(), sv.vertex().y() - pv->y(),
        sv.vertex().z() - pv->z());
    if (reco::deltaR2(jet_direction, flight_direction) > 0.09)
      continue;
    VertexState const state{
        RecoVertex::convertPos(sv.position()),
        RecoVertex::convertError(sv.error())};
    Measurement1D const dist = distance_calc.distance(*pv, state);
    // ^This is a design flaw in VertexDistance3D: this method should have been
    // static

    if (dist.significance() > max_significance) {
      max_significance = dist.significance();
      sv_mass_ = sv.mass();
      sv_pt_frac_ = sv.pt() / jet.correctedP4("Uncorrected").pt();
      sv_distance_ = dist.value();
      sv_significance_ = dist.significance();
      sv_num_tracks_ = sv.numberOfSourceCandidatePtrs();
    }
  }
}


DEFINE_FWK_MODULE(MLJECVars);
