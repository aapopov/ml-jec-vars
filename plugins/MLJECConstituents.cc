#include "MLJECConstituents.h"

#include <cassert>
#include <cmath>
#include <cstdlib>

#include <DataFormats/GeometryCommonDetAlgo/interface/Measurement1D.h>
#include <DataFormats/GeometryVector/interface/GlobalVector.h>
#include <DataFormats/Math/interface/deltaPhi.h>
#include <DataFormats/Math/interface/deltaR.h>
#include <DataFormats/PatCandidates/interface/PackedCandidate.h>
#include <FWCore/Framework/interface/MakerMacros.h>
#include <RecoVertex/VertexPrimitives/interface/ConvertToFromReco.h>
#include <RecoVertex/VertexPrimitives/interface/VertexState.h>
#include <RecoVertex/VertexTools/interface/VertexDistance3D.h>


MLJECConstituents::MLJECConstituents(edm::ParameterSet const &cfg)
    : MLJECBase(cfg) {}


void MLJECConstituents::fillDescriptions(
    edm::ConfigurationDescriptions &descriptions) {
  auto const desc = CreateDescription();
  descriptions.add("mlJECConstituents", desc);
}


void MLJECConstituents::AddBranches(TTree *tree) {
  tree->Branch("ch_size", &ch_size_);
  tree->Branch("ch_id", ch_id_, "ch_id[ch_size]/I");
  tree->Branch("ch_pt", ch_pt_, "ch_pt[ch_size]/F");
  tree->Branch("ch_eta", ch_eta_, "ch_eta[ch_size]/F");
  tree->Branch("ch_phi", ch_phi_, "ch_phi[ch_size]/F");
  tree->Branch("ch_pv_ass", ch_pv_ass_, "ch_pv_ass[ch_size]/I");
  tree->Branch("ch_dxy", ch_dxy_, "ch_dxy[ch_size]/F");
  tree->Branch("ch_dxy_significance", ch_dxy_significance_,
      "ch_dxy_significance[ch_size]/F");
  tree->Branch("ch_dz", ch_dz_, "ch_dz[ch_size]/F");
  tree->Branch("ch_num_hits", ch_num_hits_, "ch_num_hits[ch_size]/I");
  tree->Branch("ch_num_pixel_hits", ch_num_pixel_hits_,
      "ch_num_pixel_hits[ch_size]/I");
  tree->Branch("ch_lost_hits", ch_lost_hits_, "ch_lost_hits[ch_size]/I");
  tree->Branch("ch_norm_chi2", ch_norm_chi2_, "ch_norm_chi2[ch_size]/F");

  tree->Branch("ne_size", &ne_size_);
  tree->Branch("ne_id", ne_id_, "ne_id[ne_size]/I");
  tree->Branch("ne_pt", ne_pt_, "ne_pt[ne_size]/F");
  tree->Branch("ne_eta", ne_eta_, "ne_eta[ne_size]/F");
  tree->Branch("ne_phi", ne_phi_, "ne_phi[ne_size]/F");
  tree->Branch("ne_hcal_frac", ne_hcal_frac_, "ne_hcal_frac[ne_size]/F");

  tree->Branch("sv_size", &sv_size_);
  tree->Branch("sv_mass", sv_mass_, "sv_mass[sv_size]/F");
  tree->Branch("sv_pt", sv_pt_, "sv_pt[sv_size]/F");
  tree->Branch("sv_distance", sv_distance_, "sv_distance[sv_size]/F");
  tree->Branch("sv_significance", sv_significance_,
      "sv_significance[sv_size]/F");
  tree->Branch("sv_num_tracks", sv_num_tracks_, "sv_num_tracks[sv_size]/I");
}


void MLJECConstituents::FillJet(
    pat::Jet const &jet, reco::Vertex const *pv,
    edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices) {
  ch_size_ = 0;
  ne_size_ = 0;
  int const num_daughters = jet.numberOfDaughters();
  assert(num_daughters > 0);
  for (int i = 0; i < num_daughters; ++i) {
    // Constituents are sorted in decreasing order in pt
    auto const daughter = dynamic_cast<pat::PackedCandidate const *>(
        jet.daughter(i));
    if (IsCharged(*daughter)) {
      if (ch_size_ == max_size_)
        continue;
      ch_id_[ch_size_] = daughter->pdgId();
      ch_pt_[ch_size_] = daughter->pt();
      ch_eta_[ch_size_] = daughter->eta();
      ch_phi_[ch_size_] = daughter->phi();
      ch_pv_ass_[ch_size_] = int(daughter->pvAssociationQuality());
      ch_dxy_[ch_size_] = daughter->dxy();
      if (daughter->hasTrackDetails() and daughter->dxyError() != 0.)
        ch_dxy_significance_[ch_size_] = std::abs(
            daughter->dxy() / daughter->dxyError());
      else
        ch_dxy_significance_[ch_size_] = 0.;
      ch_dz_[ch_size_] = daughter->dzAssociatedPV();
      if (not std::isfinite(ch_dz_[ch_size_]))  // Somehow can be infinite
        ch_dz_[ch_size_] = 100.;
      ch_num_hits_[ch_size_] = daughter->numberOfHits();
      ch_num_pixel_hits_[ch_size_] = daughter->numberOfPixelHits();
      ch_lost_hits_[ch_size_] = int(daughter->lostInnerHits());
      if (daughter->hasTrackDetails())
        ch_norm_chi2_[ch_size_] = daughter->pseudoTrack().normalizedChi2();
      else
        ch_norm_chi2_[ch_size_] = 100.;
      ++ch_size_;
    } else {
      if (ne_size_ == max_size_)
        continue;
      ne_id_[ne_size_] = daughter->pdgId();
      ne_pt_[ne_size_] = daughter->pt();
      ne_eta_[ne_size_] = daughter->eta();
      ne_phi_[ne_size_] = daughter->phi();
      ne_hcal_frac_[ne_size_] = daughter->hcalFraction();
      ++ne_size_;
    }
  }

  FillSecondaryVertices(jet, pv, secondary_vertices);
}


void MLJECConstituents::FillSecondaryVertices(
    pat::Jet const &jet, reco::Vertex const *pv,
    edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices) {
  sv_size_ = 0;
  if (not pv) {
    return;
  }

  // Code below is adapted from [1] as recommended in [2]
  // [1] https://github.com/cms-nanoAOD/cmssw/blob/419d33be023f9f2a4c56ef4b851552d2d228600a/PhysicsTools/NanoAOD/plugins/BJetEnergyRegressionVarProducer.cc#L242-L257
  // [2] https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2017?rev=23#Secondary_Vertices_and_V0s
  GlobalVector const jet_direction(jet.px(), jet.py(), jet.pz());
  VertexDistance3D distance_calc;

  for (auto const &sv : secondary_vertices) {
    GlobalVector const flight_direction(
        sv.vertex().x() - pv->x(), sv.vertex().y() - pv->y(),
        sv.vertex().z() - pv->z());
    if (reco::deltaR2(jet_direction, flight_direction) > 0.09)
      continue;
    VertexState const state{
        RecoVertex::convertPos(sv.position()),
        RecoVertex::convertError(sv.error())};
    Measurement1D const dist = distance_calc.distance(*pv, state);
    // ^This is a design flaw in VertexDistance3D: this method should have been
    // static

    sv_mass_[sv_size_] = sv.mass();
    sv_pt_[sv_size_] = sv.pt();
    sv_distance_[sv_size_] = dist.value();
    sv_significance_[sv_size_] = dist.significance();
    sv_num_tracks_[sv_size_] = sv.numberOfSourceCandidatePtrs();

    ++sv_size_;
  }
}


DEFINE_FWK_MODULE(MLJECConstituents);
