#include "DumpJetDistr.h"

#include <cmath>
#include <cstdlib>
#include <vector>

#include <DataFormats/Common/interface/Ptr.h>
#include <DataFormats/JetReco/interface/GenJetCollection.h>
#include <FWCore/Framework/interface/MakerMacros.h>


DumpJetDistr::DumpJetDistr(edm::ParameterSet const &cfg) {
  jet_token_ = consumes<edm::View<reco::GenJet>>(
      cfg.getParameter<edm::InputTag>("jets"));
  jet_flavor_token_ = consumes<reco::JetFlavourInfoMatchingCollection>(
      cfg.getParameter<edm::InputTag>("flavors"));
}


void DumpJetDistr::analyze(edm::Event const &event, edm::EventSetup const &) {
  edm::Handle<edm::View<reco::GenJet>> jets;
  event.getByToken(jet_token_, jets);

  edm::Handle<reco::JetFlavourInfoMatchingCollection> flavor_map;
  event.getByToken(jet_flavor_token_, flavor_map);

  for (int ijet = 0; ijet < int(jets->size()); ++ijet) {
    auto const &jet = jets->at(ijet);

    auto const &flavor_info = (*flavor_map)[edm::Ptr<reco::Jet>(jets, ijet)];
    int const hadron_flavor = flavor_info.getHadronFlavour();
    int const parton_flavor = flavor_info.getPartonFlavour();
    int const comb_flavor =
        (hadron_flavor != 0) ? hadron_flavor : std::abs(parton_flavor);

    TH2 *hist;
    switch (comb_flavor) {
      case 5:
        hist = hist_b_;
        break;
      case 4:
        hist = hist_c_;
        break;
      case 21:
        hist = hist_g_;
        break;
      case 0:
        hist = hist_unknown_;
        break;
      default:
        hist = hist_uds_;
    }
    hist->Fill(jet.p(), std::abs(jet.eta()));
  }
}


void DumpJetDistr::beginJob() {
  std::vector<double> binning;
  for (double x = 0.; x < 19.95; x += 0.1)
    binning.emplace_back(x);
  for (double x = 20.; x < 6500.5; x += 1)
    binning.emplace_back(x);
  hist_b_ = file_service_->make<TH2D>(
      "b", ";Jet momentum [GeV];Jet |#eta|",
      binning.size() - 1, binning.data(), 60, 0., 6.);
  hist_c_ = file_service_->make<TH2D>(
      "c", ";Jet momentum [GeV];Jet |#eta|",
      binning.size() - 1, binning.data(), 60, 0., 6.);
  hist_uds_ = file_service_->make<TH2D>(
      "uds", ";Jet momentum [GeV];Jet |#eta|",
      binning.size() - 1, binning.data(), 60, 0., 6.);
  hist_g_ = file_service_->make<TH2D>(
      "g", ";Jet momentum [GeV];Jet |#eta|",
      binning.size() - 1, binning.data(), 60, 0., 6.);
  hist_unknown_ = file_service_->make<TH2D>(
      "unknown", ";Jet momentum [GeV];Jet |#eta|",
      binning.size() - 1, binning.data(), 60, 0., 6.);
}


void DumpJetDistr::fillDescriptions(
    edm::ConfigurationDescriptions &descriptions) {
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>("jets")->setComment("Particle-level jets.");
  desc.add<edm::InputTag>("flavors")->setComment("Map with jet flavours.");
  descriptions.add("dumpJetDistr", desc);
}


DEFINE_FWK_MODULE(DumpJetDistr);

