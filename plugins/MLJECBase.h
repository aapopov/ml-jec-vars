#ifndef ANALYSIS_MLJECVARS_MLJECBASE_H_
#define ANALYSIS_MLJECVARS_MLJECBASE_H_

#include <memory>

#include <TTree.h>

#include <Analysis/MLJECVars/interface/JetFilter.h>
#include <CommonTools/UtilAlgos/interface/TFileService.h>
#include <DataFormats/Candidate/interface/VertexCompositePtrCandidate.h>
#include <DataFormats/PatCandidates/interface/Jet.h>
#include <DataFormats/VertexReco/interface/VertexFwd.h>
#include <FWCore/Framework/interface/EDAnalyzer.h>
#include <FWCore/Framework/interface/Event.h>
#include <FWCore/ParameterSet/interface/ParameterSet.h>
#include <FWCore/ParameterSet/interface/ParameterSetDescription.h>
#include <FWCore/ServiceRegistry/interface/Service.h>
#include <FWCore/Utilities/interface/RandomNumberGenerator.h>
#include <SimDataFormats/JetMatching/interface/JetFlavourInfoMatching.h>
#include <SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h>


/**
 * \brief Base class for production of trees with features for ML JEC
 *
 * This class reads all input collections and implements looping over jets and
 * jet filtering. Basic jet- and event-level features are saved. A derived class
 * can add more branches to the output tree and fill them by implementing the
 * pure firtual methods.
 */
class MLJECBase : public edm::EDAnalyzer {
 public:
  MLJECBase(edm::ParameterSet const &cfg);

  void analyze(edm::Event const &event, edm::EventSetup const &) override final;
  void beginJob() override final;

 protected:
  /**
   * \brief Parameter set description for inputs used by this class
   *
   * A derived class should start from the returned description in its
   * fillDescriptions method.
   */
  static edm::ParameterSetDescription CreateDescription();

  /// Check if this is a charged PF candidate
  static bool IsCharged(reco::Candidate const &cand);

 private:
  /**
   * \brief Derived class should implement this method to add new branches to
   * the output tree
   */
  virtual void AddBranches(TTree *tree) = 0;

  /**
   * \brief Derived class should implement this method to compute jet features
   *
   * \param[in] jet  Reconstructed jet.
   * \param[in] pv  Pointer to the first good primary vertex. May be null.
   * \param[in] secondary_vertices  Collection of secondary vertices in this
   *   jet.
   */
  virtual void FillJet(
      pat::Jet const &jet, reco::Vertex const *pv,
      edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices)
      = 0;

  static pat::Jet const *MatchJet(
      reco::GenJet const &gen_jet, edm::View<pat::Jet> const &jets,
      double max_dr = 0.2);

  edm::EDGetTokenT<edm::View<reco::GenJet>> gen_jet_token_;
  edm::EDGetTokenT<reco::JetFlavourInfoMatchingCollection> gen_flavor_token_;
  edm::EDGetTokenT<edm::View<pat::Jet>> jet_token_;
  edm::EDGetTokenT<reco::VertexCollection> pv_token_;
  edm::EDGetTokenT<edm::View<reco::VertexCompositePtrCandidate>> sv_token_;
  edm::EDGetTokenT<double> rho_token_;
  edm::EDGetTokenT<edm::View<PileupSummaryInfo>> pileup_summary_token_;
  edm::Service<TFileService> file_service_;

  edm::Service<edm::RandomNumberGenerator> rngService_;

  /**
   * \brief Object that computes filter efficiency for jets
   *
   * Empty if the filtering is disabled.
   */
  std::unique_ptr<JetFilter> jet_filter_;

  /// Output tree
  TTree *tree_;

  // Generator-level features
  Float_t pt_gen_, eta_gen_, phi_gen_, mass_gen_;
  Int_t hadron_flavor_, parton_flavor_;

  // Global event-level features
  Int_t num_pv_;
  Float_t rho_, exp_pileup_;

  // Basic properties of the jet
  Float_t pt_, pt_l1corr_, pt_full_corr_, eta_, phi_, mass_, area_;
};

#endif  // ANALYSIS_MLJECVARS_MLJECBASE_H_
