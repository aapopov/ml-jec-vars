#ifndef ANALYSIS_MLJECVARS_MLJECVARS_H_
#define ANALYSIS_MLJECVARS_MLJECVARS_H_

#include <array>
#include <memory>
#include <tuple>

#include <FWCore/ParameterSet/interface/ParameterSet.h>
#include <FWCore/ParameterSet/interface/ConfigurationDescriptions.h>

#include "MLJECBase.h"


/// Plugin that saves high-level features of jets
class MLJECVars : public MLJECBase {
 public:
  MLJECVars(edm::ParameterSet const &cfg);
  static void fillDescriptions(edm::ConfigurationDescriptions &descriptions);

 private:
  void AddBranches(TTree *tree) override;

  /**
   * \brief Compute jet semiaxes in the (eta, phi) plane
   *
   * Return major and minor semiaxes, in this order.
   */
  static std::tuple<double, double> EtaPhiAxes(pat::Jet const &jet);

  void FillEnergyRings(pat::Jet const &jet);

  void FillJet(
      pat::Jet const &jet, reco::Vertex const *pv,
      edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices)
      override;

  /**
   * \brief Compute a version of the beta variable used for pileup ID
   *
   * This is the fraction of the scalar sum of pt of charged constituents
   * associated with the leading primary vertex.
   */
  static double PileupBeta(pat::Jet const &jet);

  /// Compute magnitude of the pull
  static double PullMagnitude(pat::Jet const &jet);

  /**
   * \brief Save properties of the secondary vertex within dR < 0.3 with the
   * highest significance
   *
   * These features are set to zeros when no secondary vertices are found within
   * the cone or if the primary vertex is not available (the corresponding
   * pointer is null).
   */
  void SaveSecondaryVertex(
      pat::Jet const &jet, reco::Vertex const *pv,
      edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices);

  // Constituent counts and jet-level energy fractions
  Int_t mult_mu_, mult_el_, mult_ph_, mult_ch_had_, mult_ne_had_,
      mult_hf_em_, mult_hf_had_;
  Float_t energy_frac_mu_, energy_frac_el_, energy_frac_ph_,
      energy_frac_ch_had_, energy_frac_ne_had_, energy_frac_hf_em_,
      energy_frac_hf_had_;

  // Collective kinematic properties of constituents
  Float_t pt_d_, mean_dr2_, pt_frac_lead_, pt_frac_lead_ch_;
  Float_t etaphi_axis_major_, etaphi_axis_minor_;
  Float_t pileup_beta_, pull_magnitude_;

  // Energy fractions in rings around jet axis
  static constexpr std::array<double, 3> energy_rings_dr_ = {{0.1, 0.2, 0.3}};
  std::array<Float_t, energy_rings_dr_.size() + 1> energy_rings_all_,
      energy_rings_mu_, energy_rings_el_,
      energy_rings_ph_, energy_rings_ch_had_, energy_rings_ne_had_,
      energy_rings_hf_em_, energy_rings_hf_had_;

  // Properties of the secondary vertex with the highest significance
  Float_t sv_mass_, sv_pt_frac_, sv_distance_, sv_significance_;
  Int_t sv_num_tracks_;
};

#endif  // ANALYSIS_MLJECVARS_MLJECVARS_H_
