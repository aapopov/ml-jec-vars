#ifndef ANALYSIS_MLJECVARS_MLJECCONSTITUENTS_H_
#define ANALYSIS_MLJECVARS_MLJECCONSTITUENTS_H_

#include <FWCore/ParameterSet/interface/ParameterSet.h>
#include <FWCore/ParameterSet/interface/ConfigurationDescriptions.h>

#include "MLJECBase.h"


/// Plugin that saves properties of individual jet constituents
class MLJECConstituents : public MLJECBase {
 public:
  MLJECConstituents(edm::ParameterSet const &cfg);
  static void fillDescriptions(edm::ConfigurationDescriptions &descriptions);

 private:
  void AddBranches(TTree *tree) override;
  void FillJet(
      pat::Jet const &jet, reco::Vertex const *pv,
      edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices)
      override;
  void FillSecondaryVertices(
      pat::Jet const &jet, reco::Vertex const *pv,
      edm::View<reco::VertexCompositePtrCandidate> const &secondary_vertices);

  static constexpr int max_size_ = 64;

  // Properties of charged candidates
  Int_t ch_size_;
  Int_t ch_id_[max_size_];
  Float_t ch_pt_[max_size_], ch_eta_[max_size_], ch_phi_[max_size_];
  Int_t ch_pv_ass_[max_size_];
  Float_t ch_dxy_[max_size_], ch_dxy_significance_[max_size_];
  Float_t ch_dz_[max_size_];
  Int_t ch_num_hits_[max_size_], ch_num_pixel_hits_[max_size_];
  Int_t ch_lost_hits_[max_size_];
  Float_t ch_norm_chi2_[max_size_];

  // Properties of neutral candidates
  Int_t ne_size_;
  Int_t ne_id_[max_size_];
  Float_t ne_pt_[max_size_], ne_eta_[max_size_], ne_phi_[max_size_];
  Float_t ne_hcal_frac_[max_size_];

  // Properties of secondary vertices
  Int_t sv_size_;
  Float_t sv_mass_[max_size_], sv_pt_[max_size_], sv_distance_[max_size_],
      sv_significance_[max_size_];
  Int_t sv_num_tracks_[max_size_];
};

#endif  // ANALYSIS_MLJECVARS_MLJECCONSTITUENTS_H_
