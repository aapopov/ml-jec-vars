"""Saves distribution of particle-level jets in a TH2."""

import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
from RecoJets.JetProducers.ak4GenJets_cfi import ak4GenJets

process = cms.Process('Analysis')
process.path = cms.Path()
process.task = cms.Task()

options = VarParsing('analysis')
options.register(
    'includeNu', False, VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    'Indicates whether neutrinos should be included in jets'
)
options.setDefault('maxEvents', 1000)
options.setType('outputFile', VarParsing.varType.string)
options.parseArguments()

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(True)
)
process.maxEvents = cms.untracked.PSet(
    input=cms.untracked.int32(options.maxEvents))
process.load('FWCore.MessageLogger.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

process.source = cms.Source(
    'PoolSource',
    inputCommands = cms.untracked.vstring(
        'keep *', 'drop LHERunInfoProduct_*_*_*')
)
if options.inputFiles:
    process.source.fileNames = cms.untracked.vstring(options.inputFiles)
else:
    # Test file from a 'flat' QCD dataset
    process.source.fileNames = cms.untracked.vstring(
        '/store/mc/RunIISummer16MiniAODv3/QCD_Pt-15to7000_TuneCUETP8M1_Flat_13TeV_pythia8/MINIAODSIM/PUMoriond17_magnetOn_94X_mcRun2_asymptotic_v3-v2/40000/50BBFA70-AF20-E911-954D-001E67247936.root'
    )

if options.includeNu:
    gen_jet_source_label = 'packedGenParticles'
else:
    process.packedGenParticlesForJets = cms.EDFilter(
        'CandPtrSelector', src=cms.InputTag('packedGenParticles'),
        cut=cms.string('abs(pdgId) != 12 && abs(pdgId) != 14 && abs(pdgId) != 16')
    )
    process.path += process.packedGenParticlesForJets
    gen_jet_source_label = 'packedGenParticlesForJets'
process.ak4GenJets = ak4GenJets.clone(
    src=gen_jet_source_label, jetPtMin=0.)
process.task.add(process.ak4GenJets)

from PhysicsTools.JetMCAlgos.HadronAndPartonSelector_cfi import \
    selectedHadronsAndPartonsForGenJetsFlavourInfos
from PhysicsTools.JetMCAlgos.AK4GenJetFlavourInfos_cfi import \
    ak4GenJetFlavourInfos
process.selectedHadronsAndPartonsForGenJetsFlavourInfos = \
    selectedHadronsAndPartonsForGenJetsFlavourInfos
process.ak4GenJetFlavourInfos = ak4GenJetFlavourInfos.clone(
    jets='ak4GenJets')
process.task.add(
    process.selectedHadronsAndPartonsForGenJetsFlavourInfos,
    process.ak4GenJetFlavourInfos
)

process.promptLeptons = cms.EDFilter(
    'CandViewSelector',
    src=cms.InputTag('prunedGenParticles'),
    cut=cms.string(
        '(abs(pdgId) == 11 | abs(pdgId) == 13 | abs(pdgId) == 15) & '
        'statusFlags().isPrompt'
    )
)
process.leptonVeto = cms.EDFilter(
    'PATCandViewCountFilter',
    src=cms.InputTag('promptLeptons'),
    minNumber=cms.uint32(0),
    maxNumber=cms.uint32(0)
)
process.path += cms.Sequence(process.promptLeptons + process.leptonVeto)

process.jetDistr = cms.EDAnalyzer(
    'DumpJetDistr',
    jets=cms.InputTag('ak4GenJets'),
    flavors=cms.InputTag('ak4GenJetFlavourInfos')
)
process.path += process.jetDistr

process.path.associate(process.task)
process.TFileService = cms.Service(
    'TFileService', fileName=cms.string(options.outputFile))

