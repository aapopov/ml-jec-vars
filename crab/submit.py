#!/usr/bin/env python

"""Constructs and submits CRAB tasks for all datasets."""

from __future__ import print_function
import argparse
import copy
from httplib import HTTPException
import imp
import shutil
import multiprocessing

import yaml

from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException


def build_config(template, task_name, dataset, task_config):
    """Construct CRAB configuration.

    Arguments:
        template:     Template CRAB configuration.
        task_name:    Name for the CRAB task.
        dataset:      Dataset on which to run.
        task_config:  Task-specific adjustments to the configuration.

    Return value:
        An instance of CRAB configuration.
    """

    config = copy.deepcopy(template)
    config.General.requestName = task_name
    config.Data.inputDataset = dataset

    # Combine pyCfgParams from the template and task configurations
    py_cfg_params = []
    if hasattr(config.JobType, 'pyCfgParams'):
        py_cfg_params += config.JobType.pyCfgParams
    if 'py_cfg_params' in task_config:
        py_cfg_params += task_config['py_cfg_params']
    config.JobType.pyCfgParams = py_cfg_params

    return config


def submit(config):
    """Submit CRAB task for given configuration.

    If the submission fails, try again for a few times.
    """

    task_name = config.General.requestName
    print('Submitting task "\033[1;34m{}\033[0m"...'.format(task_name))
    cur_attempt = 0
    num_attempts = 3
    try_submit = True

    while try_submit:
        cur_attempt += 1
        try_submit = False

        try:
            crabCommand('submit', config=config)
        except HTTPException as e:
            print('\033[1;38;5;208mFailed to submit\033[0m '
                  'because of an HTTP exception:')
            print(' ', str(e.headers))

            if cur_attempt <= num_attempts:
                print('Going to try again (attempt {} of {})...'.format(
                    cur_attempt, num_attempts))
                shutil.rmtree('crab_' + task_name)
                try_submit = True
            else:
                print('\033[1;31mGiving up on this task\033[0m')
        except ClientException as e:
            print('\033[01;31mFailed to submit\033[0m '
                  'because of a client exception:')
            print(' ', str(e))
        print()


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('config', help='Base CRAB configuration.')
    arg_parser.add_argument('samples',
                            help='YAML file with samples to process.')
    args = arg_parser.parse_args()

    with open(args.samples) as f:
        task_blocks = yaml.safe_load(f)
    template_config = imp.load_source('config', args.config).config

    for name, task_block in task_blocks.items():
        for i, dataset in enumerate(task_block['datasets']):
            task_name = '{}_p{}'.format(name, i + 1)
            config = build_config(
                template_config, task_name, dataset, task_block)

            # Run submission in a dedicated process to work around the
            # caching problem described in [1]
            # [1] https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3FAQ?rev=110#Multiple_submission_fails_with_a
            process = multiprocessing.Process(target=submit, args=(config,))
            process.start()
            process.join()

