# Template CRAB configuration

import os.path
from CRABClient.UserUtilities import config

config = config()

config.General.requestName = ''
config.General.transferOutputs = True
config.General.transferLogs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = os.path.expandvars(
    '$CMSSW_BASE/src/Analysis/MLJECVars/python/run_cfg.py')
config.JobType.pyCfgParams = []

config.Data.inputDataset = ''
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 5
config.Data.publication = False
config.Data.outLFNDirBase = '/store/user/aapopov/stageout/JME200316/'

config.Site.storageSite = 'T2_BE_IIHE'

config.User.voGroup = 'becms'

