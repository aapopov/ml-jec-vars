/**
 * \file partition.cc
 *
 * This program reads trees from multiple source ROOT files, shuffles their
 * entries, and splits output into files with given number of entries in each.
 * The shuffling algorithm assumes that entries within each source tree do not
 * need to be shuffled.
 */

#include <cmath>
#include <iomanip>
#include <iostream>
#include <memory>
#include <random>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <TFile.h>
#include <TTree.h>

namespace fs = boost::filesystem;
namespace po = boost::program_options;
using namespace std::string_literals;


/// A single source tree
class Source {
 public:
  /// Construct from a file path
  Source(fs::path const &path, std::string const &tree_name)
    : file_{TFile::Open(path.c_str())},
      tree_{dynamic_cast<TTree *>(file_->Get(tree_name.c_str()))},
      cur_entry_{0} {
    if (not file_ or file_->IsZombie()) {
      std::ostringstream message;
      message << "Failed to open file " << path << ".";
      throw std::runtime_error(message.str());
    }
    if (not tree_) {
      std::ostringstream message;
      message << "File " << path << " does not contain tree \"" << tree_name
          << "\".";
      throw std::runtime_error(message.str());
    }
  }

  TTree *Clone() {
    return tree_->CloneTree(0);
  }

  /// Set addresses of the given tree to those of the underlying source tree
  void CopyAddresses(TTree *consumer) {
    tree_->CopyAddresses(consumer);
  }

  /// Number of entries in the underlying tree
  int64_t NumEntries() const {
    return tree_->GetEntries();
  }

  /// Read next entry
  void ReadNext() {
    tree_->GetEntry(cur_entry_);
    ++cur_entry_;
  }

 private:
  std::unique_ptr<TFile> file_;
  TTree *tree_;
  int64_t cur_entry_;
};


int main(int argc, char **argv) {
  po::options_description options_description{"Supported arguments"};
  options_description.add_options()
      ("help,h", "Prints help message")
      ("sources", po::value<std::vector<fs::path>>(), "Source files (required)")
      ("tree-name,t",
       po::value<std::string>()->default_value("mlJECConstituents/Jets"),
       "Name of tree.")
      ("output,o", po::value<fs::path>()->default_value("data"),
       "Output directory.")
      ("split,s", po::value<int64_t>()->default_value(500'000),
       "Number of entries per single output file.")
      ("equalize,e", "Adjust given number of entries per file so that all "
       "files contain similar numbers of entries.")
      ("no-shuffle,n", "Do not shuffle entries.");
  po::positional_options_description positional_options;
  positional_options.add("sources", -1);
  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(options_description)
          .positional(positional_options).run(),
      options);
  po::notify(options);

  if (options.count("help")) {
    std::cerr << "Shuffles entries in input files and merges them into blocks "
        << "of given size.\n";
    std::cerr << "Usage: partition sources [options]\n";
    std::cerr << options_description << std::endl;
    return EXIT_FAILURE;
  }

  if (not options.count("sources")) {
    std::cerr << "Required argument sources is missing." << std::endl;
    return EXIT_FAILURE;
  }

  auto const tree_name = options["tree-name"].as<std::string>();
  std::vector<Source> sources;
  for (auto const &path : options["sources"].as<std::vector<fs::path>>())
    sources.emplace_back(path, tree_name);


  // For each jet written to an output file will choose the source randomly.
  // Create a vector of indices of sources and shuffle it.
  int64_t num_entries_total = 0;
  for (auto const &source : sources)
    num_entries_total += source.NumEntries();
  std::vector<int> source_indices;
  source_indices.reserve(num_entries_total);
  std::cout << "Will read " << num_entries_total << " entries from "
      << sources.size() << " source files." << std::endl;

  for (int isource = 0; isource < int(sources.size()); ++isource) {
    int64_t const n = sources[isource].NumEntries();
    for (int64_t i = 0; i < n; ++i)
      source_indices.push_back(isource);
  }

  if (not options.count("no-shuffle")) {
    std::mt19937 rng_engine{5076};
    std::shuffle(source_indices.begin(), source_indices.end(), rng_engine);
  }


  auto entries_per_file = options["split"].as<int64_t>();
  int num_output_files;
  if (entries_per_file <= 0) {
    entries_per_file = num_entries_total;
    num_output_files = 1;
  } else {
    if (options.count("equalize")) {
      num_output_files = std::lround(
          num_entries_total / double(entries_per_file));
      entries_per_file = (num_entries_total + num_output_files - 1)
          / num_output_files;  // Integer ceiling
    } else {
      num_output_files = (num_entries_total + entries_per_file - 1)
          / entries_per_file;
    }
  }

  auto const output_dir = options["output"].as<fs::path>();
  fs::create_directories(output_dir);

  std::unique_ptr<TFile> output_file;
  TTree *output_tree{nullptr};  // Will be owned by output_file
  for (int output_file_index = 0; output_file_index < num_output_files;
       ++output_file_index) {
    std::ostringstream output_path;
    output_path << output_dir.string() << "/" << std::setfill('0')
        << std::setw(std::floor(std::log10(num_output_files)) + 1)
        << output_file_index + 1 << ".root";
    output_file.reset(new TFile(output_path.str().c_str(), "recreate"));
    output_tree = sources[0].Clone();
    output_tree->SetDirectory(output_file.get());
    output_tree->SetAutoSave(0);  // Don't create multiple cycles for the tree

    for (int64_t ientry = output_file_index * entries_per_file;
         ientry < (output_file_index + 1) * entries_per_file
         and ientry < num_entries_total; ++ientry) {
      auto &source = sources[source_indices[ientry]];
      source.CopyAddresses(output_tree);
      source.ReadNext();
      output_tree->Fill();
    }

    output_file->Write();
    output_file->Close();
    std::cout << "Produced " << output_file_index + 1 << " / "
        << num_output_files << " output files." << std::endl;
  }

  return EXIT_SUCCESS;
}

