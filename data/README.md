# Data files

* `jet_filtering.root` <br />
  2D histograms with (logarithm of the) target jet filtering efficiencies. They are tuned for use with all the QCD H<sub>T</sub> samples.

