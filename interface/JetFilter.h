#ifndef ANALYSIS_MLJECVARS_JETFILTER_H_
#define ANALYSIS_MLJECVARS_JETFILTER_H_

#include <memory>
#include <string>

#include <DataFormats/JetReco/interface/GenJet.h>


class Interpolator;


/**
 * \brief Provides desired efficiency of jet selection
 *
 * The efficiency is parameterized by jet flavour and kinematics.  Jets of
 * unknown flavour and gluon jets are combined together.
 */
class JetFilter {
 public:
  /**
   * \brief Constructor from ROOT file with histograms
   *
   * The file whose path is provided as the argument must contain 2D histograms
   * for b, c, uds, and g jets.
   */
  JetFilter(std::string const &path);

  ~JetFilter();

  /// Computes probability with which the given jet should be kept
  double GetEfficiency(reco::GenJet const &jet, int hadron_flavor,
                       int parton_flavor) const;

 private:
  std::unique_ptr<Interpolator> interp_b_, interp_c_, interp_uds_, interp_g_;
};

#endif  // ANALYSIS_MLJECVARS_JETFILTER_H_

