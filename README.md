# Inputs for ML jet calibration

This package produces files with inputs for jet calibration with neural networks, as well as some additional generator-level properties to study performance of the procedure. It also applies a downsampling of jets in overpopolated regions of the parameter space, as discussed in [this talk](https://indico.cern.ch/event/924584/#10-pt-spectrum-in-dnn-jet-cali).

Two versions of inputs can be produced, each with a dedicated plugin:
* `MLJECVars` plugin saves high-level features, which were heavily inspired by [HIG-18-027](http://cms-results.web.cern.ch/cms-results/public-results/publications/HIG-18-027/index.html) and [JME-18-001](https://cds.cern.ch/record/2683784).
* `MLJECConstituents` plugin saves basic jet- and event-level features and properties of all individual constituent PF candidates and secondary vertices associated with the jet.


## Technical instructions

The package uses `CMSSW_9_4_16`. After creating the CMSSW area, set up the package with
```sh
cd $CMSSW_BASE/src
mkdir Analysis; cd Analysis
git clone ssh://git@gitlab.cern.ch:7999/aapopov/ml-jec-vars.git MLJECVars
cd MLJECVars
scram b
```
Run a test job with
```sh
cmsRun python/run_cfg.py
```
By default, `MLJECConstituents` plugin is run. To produce the high-level features instead, use the corresponding command line option.

After the processing in Grid has been finished, merge and shuffle outputs of different jobs with a command like
```sh
partition crab_QCD_HT*/results/*.root -o shards -s 100000
```
